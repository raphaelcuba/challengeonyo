package com.onyo.challengeonyo.services.response_builders;


import com.onyo.challengeonyo.models.Image;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ImageBuilderTest extends TestCase {

    String stringImageJsonMocked, stringImagesJsonMocked;
    JSONObject imageJsonMocked;
    JSONArray imagesJsonMocked;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        stringImageJsonMocked = "{\"url\": \"http://images.onyo.com/EAFB_x5TwoylEoLfldZ4HIgw-r0=/0x0:160x160/fit-in/240x240/https://onyo.s3.amazonaws.com/picture/837dc8b6785a42f9a88ff45fe81e9845.jpg\", \"width\": 160, \"context\": \"company-thumbnail-small\", \"height\": 160}";
        imageJsonMocked = new JSONObject(stringImageJsonMocked);
        stringImagesJsonMocked = "[" + stringImageJsonMocked + "]";
        imagesJsonMocked = new JSONArray(stringImagesJsonMocked);
    }

    public void testBuildOneImage() {
        ImageBuilder imageBuilder = new ImageBuilder();
        try {
            Image imageBuild = imageBuilder.buildOne(imageJsonMocked);
            Image imageExpected = new Image(
                    "http://images.onyo.com/EAFB_x5TwoylEoLfldZ4HIgw-r0=/0x0:160x160/fit-in/240x240/https://onyo.s3.amazonaws.com/picture/837dc8b6785a42f9a88ff45fe81e9845.jpg",
                    "company-thumbnail-small",
                    160,
                    160
            );

            assertEquals("Image build should be equal to image expected", imageBuild.toString(), imageExpected.toString());
        } catch (JSONException e) {
            fail();
        }
    }

    public void testBuildAllImage() {
        ImageBuilder imageBuilder = new ImageBuilder();
        try {
            ArrayList<Image> imagesBuild = imageBuilder.buildAll(imagesJsonMocked);
            Image imageExpected = new Image(
                    "http://images.onyo.com/EAFB_x5TwoylEoLfldZ4HIgw-r0=/0x0:160x160/fit-in/240x240/https://onyo.s3.amazonaws.com/picture/837dc8b6785a42f9a88ff45fe81e9845.jpg",
                    "company-thumbnail-small",
                    160,
                    160
            );

            ArrayList<Image> images = new ArrayList();
            images.add(imageExpected);

            assertEquals("Images build should be equal to images expected", imagesBuild.toString(), images.toString());
        } catch (JSONException e) {
            fail();
        }
    }

    public void testThrowsErrorIfKeyIsNull() {
        ImageBuilder imageBuilder = new ImageBuilder();
        stringImageJsonMocked = "{\"width\": 160, \"context\": \"company-thumbnail-small\", \"height\": 160}";

        try {
            imageJsonMocked = new JSONObject(stringImageJsonMocked);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            imageBuilder.buildOne(imageJsonMocked);
            fail("Shoud throws an error");
        } catch (JSONException e) {
            assertTrue("When image build fail should throws an error", true);
        }
    }
}
