package com.onyo.challengeonyo.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;

import com.onyo.challengeonyo.R;
import com.onyo.challengeonyo.adapters.CompanyAdapter;
import com.onyo.challengeonyo.models.Category;
import com.onyo.challengeonyo.models.Company;
import com.onyo.challengeonyo.services.OnyoService;
import com.onyo.challengeonyo.services.response_builders.CategoryBuilder;
import com.onyo.challengeonyo.services.response_builders.CompanyBuilder;
import com.onyo.challengeonyo.view_holders.CompanyViewHolder;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements CompanyAdapter.OnItemClickListener {

    @Bind(R.id.recycler_view_companies)
    RecyclerView recyclerView;
    JSONObject responseJSON;
    private CompanyAdapter adapter;
    private MainActivity currentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        removeElevationToolbar();

        this.currentActivity = this;

        configRecyclerView();
        retrieveData();
    }

    private void removeElevationToolbar() {
        ActionBar toolbar = getSupportActionBar();
        if (toolbar != null) {
            toolbar.setElevation(0);
        }
    }

    private void configRecyclerView () {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void retrieveData() {
        OnyoService.get("/v1/mobile/brand/1/company", new Callback() {
            @Override
            public void onFailure(Request request, IOException exception) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    CompanyBuilder builder = new CompanyBuilder();
                    responseJSON = new JSONObject(response.body().string());
                    List<Company> companies = builder.buildAll(responseJSON.getJSONArray("companies"));

                    adapter = new CompanyAdapter(companies, getContext());
                    adapter.setOnItemClickListener(currentActivity);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setAdapter(adapter);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(CompanyViewHolder item, int position) {
        Intent intent = new Intent(getContext(), CategoryActivity.class);
        try {
            CategoryBuilder builder = new CategoryBuilder();
            ArrayList<Category> categories = builder.buildAll(responseJSON.getJSONArray("categories"));
            intent.putExtra("categories", categories);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
