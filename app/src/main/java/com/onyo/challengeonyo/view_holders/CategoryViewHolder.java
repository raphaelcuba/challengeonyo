package com.onyo.challengeonyo.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyo.challengeonyo.R;
import com.onyo.challengeonyo.adapters.CategoryAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CategoryAdapter parent;
    @Bind(R.id.text_view_category_name)
    TextView textViewCategoryName;

    @Bind(R.id.image_view_category)
    ImageView imageViewCategory;

    public CategoryViewHolder(View view, CategoryAdapter parent) {
        super(view);
        ButterKnife.bind(this, view);

        view.setOnClickListener(this);
        this.parent = parent;
    }

    public TextView getTextViewCategoryName() {
        return textViewCategoryName;
    }

    public ImageView getImageViewCategory() {
        return imageViewCategory;
    }

    @Override
    public void onClick(View v) {
        final CategoryAdapter.OnItemClickListener listener = this.parent.getOnItemClickListener();
        if (listener != null) {
            listener.onItemClick(this, getPosition());
        }
    }
}
