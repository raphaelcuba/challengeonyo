package com.onyo.challengeonyo.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onyo.challengeonyo.R;
import com.onyo.challengeonyo.adapters.CompanyAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CompanyAdapter parent;
    @Bind(R.id.text_view_address)
    TextView textViewAddress;

    @Bind(R.id.text_view_place)
    TextView textViewPlace;

    @Bind(R.id.image_view_company)
    ImageView imageViewCompany;


    public CompanyViewHolder(View view, CompanyAdapter parent) {
        super(view);
        ButterKnife.bind(this, view);

        view.setOnClickListener(this);
        this.parent = parent;
    }

    public TextView getTextViewAddress() {
        return textViewAddress;
    }

    public TextView getTextViewPlace() {
        return textViewPlace;
    }

    public ImageView getImageViewCompany() {
        return imageViewCompany;
    }

    @Override
    public void onClick(View v) {
        final CompanyAdapter.OnItemClickListener listener = this.parent.getOnItemClickListener();
        if (listener != null) {
            listener.onItemClick(this, getPosition());
        }
    }
}
