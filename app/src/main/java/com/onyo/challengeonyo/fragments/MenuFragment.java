package com.onyo.challengeonyo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyo.challengeonyo.R;
import com.onyo.challengeonyo.adapters.CategoryAdapter;
import com.onyo.challengeonyo.models.Category;
import com.onyo.challengeonyo.view_holders.CategoryViewHolder;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MenuFragment extends Fragment implements CategoryAdapter.OnItemClickListener {

    @Bind(R.id.recycler_view_categories)
    RecyclerView recyclerView;
    private CategoryAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);

        configRecyclerView();
        retrieveData();


        return view;
    }

    private void configRecyclerView () {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(
                2, StaggeredGridLayoutManager.VERTICAL
        );
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
    }

    private void retrieveData() {
        Bundle bundle = getActivity().getIntent().getExtras();
        ArrayList<Category> categories = bundle.getParcelableArrayList("categories");
        adapter = new CategoryAdapter(categories, getActivity());
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(CategoryViewHolder item, int position) {}

}
