package com.onyo.challengeonyo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyo.challengeonyo.R;
import com.onyo.challengeonyo.models.Category;
import com.onyo.challengeonyo.models.Company;
import com.onyo.challengeonyo.models.Image;
import com.onyo.challengeonyo.view_holders.CategoryViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private List<Category> categories;
    private OnItemClickListener mOnItemClickListener;
    private Context context;

    public CategoryAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    public interface OnItemClickListener {
        public void onItemClick(CategoryViewHolder item, int position);
    }

    public OnItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder categoryViewHolder, int index) {
        Category category = categories.get(index);
        categoryViewHolder.getTextViewCategoryName().setText(category.getName());
        Image imageCategory = category.getImageCategoryBackground();

        if (imageCategory != null) {
            Picasso.with(this.context)
                    .load(imageCategory.getUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(categoryViewHolder.getImageViewCategory());

        }
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_view_category, viewGroup, false);

        return new CategoryViewHolder(itemView, this);
    }
}