package com.onyo.challengeonyo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onyo.challengeonyo.R;
import com.onyo.challengeonyo.models.Company;
import com.onyo.challengeonyo.models.Image;
import com.onyo.challengeonyo.view_holders.CompanyViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyViewHolder> {

    private List<Company> companies;
    private OnItemClickListener mOnItemClickListener;
    private Context context;

    public CompanyAdapter(List<Company> companies, Context context) {
        this.companies = companies;
        this.context = context;
    }

    public interface OnItemClickListener {
        public void onItemClick(CompanyViewHolder item, int position);
    }

    public OnItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    @Override
    public void onBindViewHolder(CompanyViewHolder companyViewHolder, int index) {
        Company company = companies.get(index);
        companyViewHolder.getTextViewAddress().setText(company.getAddress() + " ~ 250");
        companyViewHolder.getTextViewPlace().setText(company.getName());
        Image mainImage = company.getImageMain();

        if (mainImage != null) {
            Picasso.with(this.context)
                    .load(mainImage.getUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(companyViewHolder.getImageViewCompany());

        }
    }

    @Override
    public CompanyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_view_company, viewGroup, false);

        return new CompanyViewHolder(itemView, this);
    }
}