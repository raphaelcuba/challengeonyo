package com.onyo.challengeonyo.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.onyo.challengeonyo.fragments.HomeFragment;
import com.onyo.challengeonyo.fragments.MenuFragment;
import com.onyo.challengeonyo.fragments.RescueFragment;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence titles[];
    private int numberOfTabs;

    public ViewPagerAdapter(FragmentManager fm, CharSequence titles[], int numberOfTabs) {
        super(fm);
        this.titles = titles;
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new MenuFragment();
                break;
            case 2:
                fragment = new RescueFragment();
                break;
            default:
                fragment = new HomeFragment();
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}