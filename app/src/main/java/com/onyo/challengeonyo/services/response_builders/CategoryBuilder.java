package com.onyo.challengeonyo.services.response_builders;


import com.onyo.challengeonyo.models.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CategoryBuilder {
    public Category buildOne(JSONObject categoryJSON) throws JSONException {
        Category category = new Category();
        category.setBrand(categoryJSON.getString("brand"));
        category.setId(categoryJSON.getString("id"));
        category.setMenu(categoryJSON.getString("menu"));
        category.setName(categoryJSON.getString("name"));
        category.setOrder(categoryJSON.getInt("order"));
        category.setNumericalId(categoryJSON.getInt("numericalId"));

        ImageBuilder imageBuilder = new ImageBuilder();

        if (!categoryJSON.isNull("image")) {
            JSONArray imagesJSON = categoryJSON.getJSONArray("image");
            category.setImages(imageBuilder.buildAll(imagesJSON));
        }

        return category;
    }

    public ArrayList<Category> buildAll(JSONArray categoriesJSON) throws JSONException {
        ArrayList<Category> categories = new ArrayList();

        for (int i = 0, l = categoriesJSON.length(); i < l; i++) {
            JSONObject categoryJSON = categoriesJSON.getJSONObject(i);
            categories.add(buildOne(categoryJSON));
        }

        return categories;
    }
}
