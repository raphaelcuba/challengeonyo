package com.onyo.challengeonyo.services;


import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Callback;


public class OnyoService {
    private static final String BASE_URL = "http://api.staging.onyo.com";
    private static OkHttpClient client = new OkHttpClient();

    public static void get(String path, Callback callback) {
        client.newCall(buildRequest(path)).enqueue(callback);
    }

    private static String getAbsoluteUrl(String path) {
        return String.format("%s%s", BASE_URL, path);
    }

    private static Request buildRequest(String path) {
        String url = getAbsoluteUrl(path);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "application/json")
                .build();
        return request;
    }
}
