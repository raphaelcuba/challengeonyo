package com.onyo.challengeonyo.services.response_builders;

import com.onyo.challengeonyo.models.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ImageBuilder {

    public Image buildOne(JSONObject imageJSON) throws JSONException {
        Image image = new Image();
        image.setContext(imageJSON.getString("context"));
        image.setHeight(imageJSON.getInt("height"));
        image.setWidth(imageJSON.getInt("width"));
        image.setUrl(imageJSON.getString("url"));
        return image;
    }

    public ArrayList<Image> buildAll(JSONArray imagesJSON) throws JSONException {
        ArrayList<Image> images = new ArrayList();
        for (int i = 0, l = imagesJSON.length(); i < l; i++) {
            images.add(buildOne(imagesJSON.getJSONObject(i)));
        }

        return images;
    }
}
