package com.onyo.challengeonyo.services.response_builders;

import com.onyo.challengeonyo.models.Company;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CompanyBuilder {

    public Company buildOne(JSONObject companyJSON) throws JSONException {
        Company company = new Company();
        company.setNumericalId(companyJSON.getInt("numericalId"));
        company.setName(companyJSON.getString("name"));
        company.setDiplayName(companyJSON.getString("displayName"));
        company.setAddress(companyJSON.getString("address"));
        company.setMenu(companyJSON.getString("menu"));
        company.setBrand(companyJSON.getString("brand"));
        company.setGeoLat(companyJSON.getString("geoLat"));
        company.setGeoLon(companyJSON.getString("geoLon"));
        company.setId(companyJSON.getString("id"));

        ImageBuilder imageBuilder = new ImageBuilder();
        company.setImages(imageBuilder.buildAll(companyJSON.getJSONArray("image")));

        return company;
    }

    public ArrayList<Company> buildAll(JSONArray companiesJSON) throws JSONException {
        ArrayList<Company> companies = new ArrayList();
        for (int i = 0, l = companiesJSON.length(); i < l; i++) {
            JSONObject companyJSON = companiesJSON.getJSONObject(i);
            companies.add(buildOne(companyJSON));
        }

        return companies;
    }
}
