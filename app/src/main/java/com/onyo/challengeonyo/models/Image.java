package com.onyo.challengeonyo.models;


import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {
    private String url, context;
    private int width, height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Image(String url, String context, int width, int height) {
        this.url = url;
        this.context = context;
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Image{" +
                "url='" + url + '\'' +
                ", context='" + context + '\'' +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.context);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    public Image() {}

    protected Image(Parcel in) {
        this.url = in.readString();
        this.context = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}