package com.onyo.challengeonyo.models;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Company implements Parcelable {
    private int numericalId;
    private String name, diplayName, address, menu, brand, id, geoLat, geoLon;
    private List<Image> images;

    public Image getImageMain() {
        for (Image image: images) {
            if (image.getContext().equals("company-main")) {
                return image;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumericalId() {
        return numericalId;
    }

    public void setNumericalId(int numericalId) {
        this.numericalId = numericalId;
    }

    public String getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(String geoLat) {
        this.geoLat = geoLat;
    }

    public String getGeoLon() {
        return geoLon;
    }

    public void setGeoLon(String geoLon) {
        this.geoLon = geoLon;
    }

    public String getDiplayName() {
        return diplayName;
    }

    public void setDiplayName(String diplayName) {
        this.diplayName = diplayName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }



    @Override
    public String toString() {
        return "Company{" +
                "numericalId=" + numericalId +
                ", name='" + name + '\'' +
                ", diplayName='" + diplayName + '\'' +
                ", address='" + address + '\'' +
                ", menu='" + menu + '\'' +
                ", brand='" + brand + '\'' +
                ", id='" + id + '\'' +
                ", geoLat='" + geoLat + '\'' +
                ", geoLon='" + geoLon + '\'' +
                ", images=" + images +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.numericalId);
        dest.writeString(this.name);
        dest.writeString(this.diplayName);
        dest.writeString(this.address);
        dest.writeString(this.menu);
        dest.writeString(this.brand);
        dest.writeString(this.id);
        dest.writeString(this.geoLat);
        dest.writeString(this.geoLon);
        dest.writeTypedList(images);
    }

    public Company() {
    }

    protected Company(Parcel in) {
        this.numericalId = in.readInt();
        this.name = in.readString();
        this.diplayName = in.readString();
        this.address = in.readString();
        this.menu = in.readString();
        this.brand = in.readString();
        this.id = in.readString();
        this.geoLat = in.readString();
        this.geoLon = in.readString();
        this.images = in.createTypedArrayList(Image.CREATOR);
    }

    public static final Parcelable.Creator<Company> CREATOR = new Parcelable.Creator<Company>() {
        public Company createFromParcel(Parcel source) {
            return new Company(source);
        }

        public Company[] newArray(int size) {
            return new Company[size];
        }
    };
}
