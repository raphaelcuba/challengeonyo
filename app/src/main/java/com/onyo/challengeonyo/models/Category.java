package com.onyo.challengeonyo.models;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Category implements Parcelable {
    private int numericalId, order;
    private String name, menu, brand, id;
    private List<Image> images;

    public int getNumericalId() {
        return numericalId;
    }

    public void setNumericalId(int numericalId) {
        this.numericalId = numericalId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Image getImageCategoryBackground() {
        if (images != null) {
            for (Image image: images) {
                if (image.getContext().equals("category-background")) {
                    return image;
                }
            }
        }
        return null;
    }


    @Override
    public String toString() {
        return "Category{" +
                "numericalId=" + numericalId +
                ", order=" + order +
                ", name='" + name + '\'' +
                ", menu='" + menu + '\'' +
                ", brand='" + brand + '\'' +
                ", id='" + id + '\'' +
                ", images=" + images +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.numericalId);
        dest.writeInt(this.order);
        dest.writeString(this.name);
        dest.writeString(this.menu);
        dest.writeString(this.brand);
        dest.writeString(this.id);
        dest.writeTypedList(images);
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.numericalId = in.readInt();
        this.order = in.readInt();
        this.name = in.readString();
        this.menu = in.readString();
        this.brand = in.readString();
        this.id = in.readString();
        this.images = in.createTypedArrayList(Image.CREATOR);
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
