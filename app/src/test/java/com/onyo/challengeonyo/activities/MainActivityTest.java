package com.onyo.challengeonyo.activities;


import android.content.Intent;

import com.onyo.challengeonyo.BuildConfig;
import com.onyo.challengeonyo.R;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainActivityTest {

    @Test
    public void clickingLogin_shouldStartCompanyActivity() {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
        activity.findViewById(R.id.card_view).performClick();

        Intent expectedIntent = new Intent(activity, CategoryActivity.class);
        boolean result = shadowOf(activity).getNextStartedActivityForResult().equals(expectedIntent);
        Assert.assertEquals("Call Activity Category", result);
    }

}
